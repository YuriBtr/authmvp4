package yuribtr.skillbranch.ru.mvpauth4.mvp.presenters;

import android.net.Uri;

import mortar.MortarScope;
import yuribtr.skillbranch.ru.mvpauth4.mvp.views.IRootView;

public class RootPresenter extends AbstractPresenter<IRootView> {
    private IImageChooseNotifier mNotifier = null;

    @Override
    public void initView(MortarScope scope) {
        if (getView() != null) {
            getView().setMenuItemChecked(scope.getName());
        }
    }

    public void loadPhotoFromGallery(IImageChooseNotifier notifier) {
        if (getView() != null) {
            mNotifier = notifier;
            getView().requestImageFromGallery();
        }
    }

    public void loadPhotoFromCamera(IImageChooseNotifier notifier) {
        if (getView() != null) {
            mNotifier = notifier;
            getView().requestImageFromCamera();
        }
    }

    public void onPhotoLoaded(Uri uri, byte from) {
        if (mNotifier != null) {
            mNotifier.onImageChoose(uri);
            mNotifier = null;
        }
    }


}
