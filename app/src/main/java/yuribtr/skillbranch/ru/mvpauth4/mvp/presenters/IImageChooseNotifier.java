package yuribtr.skillbranch.ru.mvpauth4.mvp.presenters;

import android.net.Uri;

public interface IImageChooseNotifier {
    void onImageChoose(Uri uri);
}
