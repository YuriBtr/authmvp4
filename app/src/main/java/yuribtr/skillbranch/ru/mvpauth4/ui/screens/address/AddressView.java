package yuribtr.skillbranch.ru.mvpauth4.ui.screens.address;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import yuribtr.skillbranch.ru.mvpauth4.R;
import yuribtr.skillbranch.ru.mvpauth4.data.storage.dto.UserAddressDto;
import yuribtr.skillbranch.ru.mvpauth4.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth4.mvp.views.IAddressView;

public class AddressView extends LinearLayout implements IAddressView {
    @BindView(R.id.add_address_apt_et)
    EditText mAddressApt;
    @BindView(R.id.add_address_comment_et)
    EditText mAddressComment;
    @BindView(R.id.add_address_house_et)
    EditText mAddressHouse;
    @BindView(R.id.add_address_name_et)
    EditText mAddressName;
    @BindView(R.id.add_address_floor_et)
    EditText mAddressFloor;
    @BindView(R.id.add_address_street_et)
    EditText mAddressStreet;

    @Inject
    AddressScreen.AddressPresenter mPresenter;

    public AddressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            DaggerService.<AddressScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    //region================LifeCycle================

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    //endregion

    //region================IAddressView================
    @Override
    public void showInputError() {
        Toast.makeText(this.getContext(), R.string.fileds_address_name_street_name_empty, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showMessage(int messageResId) {
        Toast.makeText(this.getContext(), messageResId, Toast.LENGTH_SHORT).show();
    }

    @Override
    public UserAddressDto getUserAddress() {
        int tmpFloor;
        try {
            tmpFloor = Integer.parseInt(mAddressFloor.getText().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            tmpFloor = 0;
        }
        UserAddressDto userAddressDto = new UserAddressDto(mAddressName.getText().toString(),
                mAddressStreet.getText().toString(),
                mAddressHouse.getText().toString(),
                mAddressApt.getText().toString(),
                tmpFloor,
                mAddressComment.getText().toString());
        return userAddressDto;
    }

    @Override
    public boolean viewOnBackPressed() {
        return false;
    }
    //endregion

    //region================Events================

    @OnClick(R.id.add_address_btn)
    void AddAddressBtn() {
        mPresenter.clickOnAddAddress();
    }

    //endregion

}
