package yuribtr.skillbranch.ru.mvpauth4.mvp.presenters;

public interface IProductPresenter {
    void clickOnPlus();

    void clickOnMinus();
}
