package yuribtr.skillbranch.ru.mvpauth4.di.modules;

import dagger.Provides;
import yuribtr.skillbranch.ru.mvpauth4.di.scopes.RootScope;
import yuribtr.skillbranch.ru.mvpauth4.mvp.presenters.RootPresenter;

//сначала создается модуль
@dagger.Module
public class RootModule {
    @Provides
    @RootScope
    RootPresenter provideRootPresenter() {
        return new RootPresenter();
    }
}
