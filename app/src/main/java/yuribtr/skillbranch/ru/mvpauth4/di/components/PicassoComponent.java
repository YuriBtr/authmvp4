package yuribtr.skillbranch.ru.mvpauth4.di.components;

import com.squareup.picasso.Picasso;

import dagger.Component;
import yuribtr.skillbranch.ru.mvpauth4.di.modules.PicassoCacheModule;
import yuribtr.skillbranch.ru.mvpauth4.di.scopes.RootScope;

@Component(dependencies = AppComponent.class, modules = PicassoCacheModule.class)
@RootScope
public interface PicassoComponent {
    Picasso getPicasso();
}
