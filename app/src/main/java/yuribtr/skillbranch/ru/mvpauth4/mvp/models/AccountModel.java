package yuribtr.skillbranch.ru.mvpauth4.mvp.models;

import android.net.Uri;

import java.util.ArrayList;
import java.util.Map;

import yuribtr.skillbranch.ru.mvpauth4.data.managers.ConstantManager;
import yuribtr.skillbranch.ru.mvpauth4.data.managers.PreferencesManager;
import yuribtr.skillbranch.ru.mvpauth4.data.storage.dto.UserAddressDto;
import yuribtr.skillbranch.ru.mvpauth4.data.storage.dto.UserDto;

public class AccountModel extends AbstractModel {

    public UserDto getUserDto() {
        return new UserDto(getUserProfileInfo(), getUserAddresses(), getUserSettings());
    }

    private Map<String, String> getUserProfileInfo() {
        return mDataManager.getUserProfileInfo();
    }

    private ArrayList<UserAddressDto> getUserAddresses() {
        return mDataManager.getUserAddress();
    }

    private Map<String, Boolean> getUserSettings() {
        return mDataManager.getUserSettings();
    }

    public void saveProfileInfo(String name, String phone) {
        mDataManager.saveProfileInfo(name, phone);
    }

    public void saveAvatarPhoto(Uri photoUri) {
        mDataManager.saveUserPhoto(photoUri);
    }

    public Uri loadUserPhoto() {
        return mDataManager.loadUserPhoto();
    }

    public void savePromoNotification(boolean isChecked) {
        mDataManager.saveSetting(ConstantManager.NOTIFICATION_PROMO_KEY, isChecked);
    }

    public void saveOrderNotification(boolean isChecked) {
        mDataManager.saveSetting(ConstantManager.NOTIFICATION_ORDER_KEY, isChecked);
    }

    public void removeUserAddress(int addressId) {
        mDataManager.removeUserAddress(addressId);
    }

    public void addAddress(UserAddressDto userAddressDto) {
        mDataManager.addAddress(userAddressDto);
    }

    //todo: remove address
}
