package yuribtr.skillbranch.ru.mvpauth4.di.components;

import javax.inject.Singleton;

import dagger.Component;
import yuribtr.skillbranch.ru.mvpauth4.data.managers.DataManager;
import yuribtr.skillbranch.ru.mvpauth4.di.modules.LocalModule;
import yuribtr.skillbranch.ru.mvpauth4.di.modules.NetworkModule;

//наследуемся от AppComponent чтобы получить Context для LocalModule
@Component(dependencies = AppComponent.class, modules = {NetworkModule.class, LocalModule.class})
@Singleton
public interface DataManagerComponent {
    void inject(DataManager dataManager);
}
