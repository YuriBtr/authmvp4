package yuribtr.skillbranch.ru.mvpauth4.ui.screens.address;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;
import mortar.ViewPresenter;
import yuribtr.skillbranch.ru.mvpauth4.R;
import yuribtr.skillbranch.ru.mvpauth4.data.storage.dto.UserAddressDto;
import yuribtr.skillbranch.ru.mvpauth4.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth4.di.scopes.AddressScope;
import yuribtr.skillbranch.ru.mvpauth4.flow.AbstractScreen;
import yuribtr.skillbranch.ru.mvpauth4.flow.Screen;
import yuribtr.skillbranch.ru.mvpauth4.mvp.models.AccountModel;
import yuribtr.skillbranch.ru.mvpauth4.mvp.presenters.IAddressPresenter;
import yuribtr.skillbranch.ru.mvpauth4.ui.screens.account.AccountScreen;

@Screen(R.layout.screen_add_address)
public class AddressScreen extends AbstractScreen<AccountScreen.Component> implements TreeKey{
    @Override
    public Object createScreenComponent(AccountScreen.Component parentComponent) {
        return DaggerAddressScreen_Component.builder().component(parentComponent)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return new AccountScreen();
    }

    //region================DI================
    @dagger.Module
    public class Module {
        @Provides
        @AddressScope
        AddressPresenter provideAddressPresenter() {
            return new AddressPresenter();
        }
    }

    @dagger.Component (dependencies = AccountScreen.Component.class, modules = Module.class)
    @AddressScope
    public interface Component {
        void inject (AddressPresenter presenter);
        void inject (AddressView view);
    }
    //endregion

    //region================Presenter================
    public class AddressPresenter extends ViewPresenter<AddressView> implements IAddressPresenter{

        @Inject
        AccountModel mAccountModel;

        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        @Override
        public void clickOnAddAddress() {
            if (getView() != null) {
                UserAddressDto userAddressDto = getView().getUserAddress();
                if (userAddressDto.getName().isEmpty() || userAddressDto.getStreet().isEmpty()) {
                    getView().showInputError();
                } else {
                    getView().showMessage(R.string.address_added);
                    mAccountModel.addAddress(userAddressDto);
                    Flow.get(getView()).goBack();
                }
            }
        }
    }
    //endregion


}
