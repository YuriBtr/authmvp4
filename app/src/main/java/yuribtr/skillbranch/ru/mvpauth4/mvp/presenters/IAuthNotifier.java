package yuribtr.skillbranch.ru.mvpauth4.mvp.presenters;

public interface IAuthNotifier {

    void onLoginSuccess();

    void onLoginError(String message);
}
