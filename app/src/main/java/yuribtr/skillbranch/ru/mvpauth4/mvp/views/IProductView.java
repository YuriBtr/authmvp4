package yuribtr.skillbranch.ru.mvpauth4.mvp.views;

import yuribtr.skillbranch.ru.mvpauth4.data.storage.dto.ProductDto;

public interface IProductView extends IView {
    void showProductView(ProductDto product);

    void updateProductCountView(ProductDto product);

}
