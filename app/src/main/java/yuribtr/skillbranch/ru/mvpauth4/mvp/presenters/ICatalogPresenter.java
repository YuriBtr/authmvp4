package yuribtr.skillbranch.ru.mvpauth4.mvp.presenters;

public interface ICatalogPresenter {
    void clickOnBuyButton(int position);

    boolean checkUserAuth();
}
