package yuribtr.skillbranch.ru.mvpauth4.ui.screens.account;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;

import javax.inject.Inject;

import dagger.Provides;
import flow.Flow;
import mortar.MortarScope;
import mortar.ViewPresenter;
import yuribtr.skillbranch.ru.mvpauth4.R;
import yuribtr.skillbranch.ru.mvpauth4.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth4.di.scopes.AccountScope;
import yuribtr.skillbranch.ru.mvpauth4.flow.AbstractScreen;
import yuribtr.skillbranch.ru.mvpauth4.flow.Screen;
import yuribtr.skillbranch.ru.mvpauth4.mvp.models.AccountModel;
import yuribtr.skillbranch.ru.mvpauth4.mvp.presenters.IAccountPresenter;
import yuribtr.skillbranch.ru.mvpauth4.mvp.presenters.IImageChooseNotifier;
import yuribtr.skillbranch.ru.mvpauth4.mvp.presenters.RootPresenter;
import yuribtr.skillbranch.ru.mvpauth4.mvp.views.IRootView;
import yuribtr.skillbranch.ru.mvpauth4.ui.activities.RootActivity;
import yuribtr.skillbranch.ru.mvpauth4.ui.screens.address.AddressScreen;

@Screen(R.layout.screen_account)
public class AccountScreen extends AbstractScreen<RootActivity.RootComponent> {

    private int mCustomState = 1;

    public int getCustomState() {
        return mCustomState;
    }

    public void setCustomState(int customState) {
        mCustomState = customState;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent parentComponent) {
        return DaggerAccountScreen_Component.builder()
                .rootComponent(parentComponent)
                .module(new Module())
                .build();
    }

    //region================DI================
    @dagger.Module
    public class Module {
        @Provides
        @AccountScope
        AccountModel provideAccountModel() {
            return new AccountModel();
        }

        @Provides
        @AccountScope
        AccountPresenter provideAccountPresenter() {
            return new AccountPresenter();
        }
    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @AccountScope
    public interface Component {
        void inject(AccountPresenter presenter);

        void inject(AccountView view);

        RootPresenter getRootPresenter();

        AccountModel getAccountModel();
    }

    //endregion


    //region================Presenter================
    public class AccountPresenter extends ViewPresenter<AccountView> implements IAccountPresenter, IImageChooseNotifier {

        @Inject
        RootPresenter mRootPresenter;
        @Inject
        AccountModel mAccountModel;
        private Uri mAvatarUri;


        @Override
        protected void onEnterScope(MortarScope scope) {
            super.onEnterScope(scope);
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
            //notify RootpResenter that we switched screen
            mRootPresenter.initView(scope);
        }

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            if (getView() != null) {
                getView().initView(mAccountModel.getUserDto());
            }
        }

        @Override
        public void clickOnAddress() {
            Flow.get(getView()).set(new AddressScreen());
        }

        @Override
        public void switchViewState() {
            if (getCustomState() == AccountView.EDIT_STATE && getView() != null) {
                mAccountModel.saveProfileInfo(getView().getUserName(), getView().getUserPhone());
                //mAccountModel.saveAvatarPhoto(mAvatarUri);
            }
            if (getView() != null) {
                getView().changeState();
            }
        }

        @Override
        public void switchOrder(boolean isChecked) {
            mAccountModel.saveOrderNotification(isChecked);
        }

        @Override
        public void switchPromo(boolean isChecked) {
            mAccountModel.savePromoNotification(isChecked);
        }

        @Override
        public void takePhoto() {
            if (getView() != null) {
                getView().showPhotoSourceDialog();
            }
        }

        @Override
        public void chooseCamera() {
            if (getRootView() != null) {
                //getRootView().showMessage("chooseCamera");
                mRootPresenter.loadPhotoFromCamera(this);
            }
        }

        @Override
        public void chooseGallery() {
            if (getRootView() != null) {
                //getRootView().showMessage("chooseGallery");
                mRootPresenter.loadPhotoFromGallery(this);
            }
        }

        public void onAddressDelete(int addressId) {
            mAccountModel.removeUserAddress(addressId);
            if (getRootView() != null) {
                getRootView().showMessage("Адрес удален");
            }
        }

        @Nullable
        private IRootView getRootView() {
            return mRootPresenter.getView();
        }
        //endregion

        @Override
        public void onImageChoose(Uri uri) {
            mAvatarUri = uri;
            mAccountModel.saveAvatarPhoto(mAvatarUri);
            if (getView() != null) {
                getView().initView(mAccountModel.getUserDto());
            }
            //switchViewState();
        }
    }

}
