package yuribtr.skillbranch.ru.mvpauth4.mvp.views;

public interface IView {
    boolean viewOnBackPressed();
}
