package yuribtr.skillbranch.ru.mvpauth4.ui.screens.account;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import flow.Flow;
import yuribtr.skillbranch.ru.mvpauth4.R;
import yuribtr.skillbranch.ru.mvpauth4.data.storage.dto.UserDto;
import yuribtr.skillbranch.ru.mvpauth4.di.DaggerService;
import yuribtr.skillbranch.ru.mvpauth4.mvp.views.IAccountView;
import yuribtr.skillbranch.ru.mvpauth4.ui.screens.account.adapters.AddressAdapter;
import yuribtr.skillbranch.ru.mvpauth4.utils.CircleTransform;

public class AccountView extends CoordinatorLayout implements IAccountView {

    public static final int PREVIEW_STATE = 1;
    public static final int EDIT_STATE = 0;

    @Inject
    AccountScreen.AccountPresenter mPresenter;
    @Inject
    Picasso mPicasso;
    @BindView(R.id.profile_name_txt)
    TextView profileNameTxt;
    @BindView(R.id.user_avatar_iv)
    ImageView mUserAvatar;
    @BindView(R.id.user_phone_et)
    EditText userPhoneEt;
    @BindView(R.id.user_full_name_et)
    EditText userFullNameEt;
    @BindView(R.id.profile_name_wrapper)
    LinearLayout profileNameWrapper;
    @BindView(R.id.address_list)
    RecyclerView addressList;
    @BindView(R.id.add_address_btn)
    Button addAddressBtn;
    @BindView(R.id.notification_order_sw)
    SwitchCompat notificationOrderSw;
    @BindView(R.id.notification_promo_sw)
    SwitchCompat notificationPromoSw;
    @BindView(R.id.appbar_layout)
    AppBarLayout mAppBarLayout;
    @BindView(R.id.no_address_tv)
    TextView mNoAddressTv;

    private AccountScreen mScreen;
    private UserDto mUserDto;
    private TextWatcher mWatcher;
    private AddressAdapter mAddressAdapter;

    public AccountView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            mScreen = Flow.getKey(this);
            DaggerService.<AccountScreen.Component>getDaggerComponent(context).inject(this);
        }
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        ButterKnife.bind(this);
        setUpItemTouchHelper();
    }

    private void setUpItemTouchHelper() {
        ItemTouchHelper.SimpleCallback simpleItemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }
            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
                int swipedPosition = viewHolder.getAdapterPosition();
                //put real (internal) Id of Address
                mPresenter.onAddressDelete(mUserDto.getUserAddresses().get(swipedPosition).getId());
                mUserDto.getUserAddresses().remove(swipedPosition);
                addressList.getAdapter().notifyItemRemoved(swipedPosition);
                if (mUserDto.getUserAddresses().isEmpty()) mNoAddressTv.setVisibility(VISIBLE);
            }
        };
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleItemTouchCallback);
        itemTouchHelper.attachToRecyclerView(addressList);
    }

    private void showViewFromState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            showPreviewState();
        } else {
            showEditState();
        }
    }

    @Override
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!isInEditMode()) {
            mPresenter.takeView(this);
        }
    }

    @Override
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (!isInEditMode()) {
            mPresenter.dropView(this);
        }
    }

    public void initView(UserDto user) {
        mUserDto = user;
        initProfileInfo();
        initList();
        initSettings();
        showViewFromState();
    }

    private void initSettings() {
        notificationOrderSw.setChecked(mUserDto.isOrderNotification());
        notificationPromoSw.setChecked(mUserDto.isPromoNotification());
    }

    private void initList() {
        if (!mUserDto.getUserAddresses().isEmpty()) mNoAddressTv.setVisibility(GONE);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        addressList.setLayoutManager(layoutManager);
        mAddressAdapter = new AddressAdapter(mUserDto.getUserAddresses(), null);
        addressList.swapAdapter(mAddressAdapter, true);
    }

    private void initProfileInfo() {
        profileNameTxt.setText(mUserDto.getFullname());
        userFullNameEt.setText(mUserDto.getFullname());
        userPhoneEt.setText(mUserDto.getPhone());
        mPicasso.load(Uri.parse(mUserDto.getAvatar())).resize(500, 450).centerCrop().transform(new CircleTransform()).into(mUserAvatar);
    }

    //region================IAccountView================
    @Override
    public void changeState() {
        if (mScreen.getCustomState() == PREVIEW_STATE) {
            mScreen.setCustomState(EDIT_STATE);
        } else {
            mScreen.setCustomState(PREVIEW_STATE);
        }
        showViewFromState();
    }

    @Override
    public void showEditState() {
        mWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                profileNameTxt.setText(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        profileNameWrapper.setVisibility(VISIBLE);
        userFullNameEt.addTextChangedListener(mWatcher);
        userFullNameEt.setEnabled(true);
        userFullNameEt.setFocusable(true);
        userFullNameEt.setFocusableInTouchMode(true);
        userPhoneEt.setEnabled(true);
        userPhoneEt.setFocusable(true);
        userPhoneEt.setFocusableInTouchMode(true);
        //mPicasso.load(R.drawable.ic_add_black_24dp).transform(new CircleTransform()).into(mUserAvatar);
    }

    @Override
    public void showPreviewState() {
        profileNameWrapper.setVisibility(GONE);
        userFullNameEt.setEnabled(false);
        userFullNameEt.setFocusable(false);
        userFullNameEt.setFocusableInTouchMode(false);
        userPhoneEt.setEnabled(false);
        userPhoneEt.setFocusable(false);
        userPhoneEt.setFocusableInTouchMode(false);
        userFullNameEt.removeTextChangedListener(mWatcher);
        mPicasso.load(mUserDto.getAvatar()).resize(500, 450).centerCrop().transform(new CircleTransform()).into(mUserAvatar);
    }

    @Override
    public void showPhotoSourceDialog() {
        String source[] = {"Загрузить из галлереи", "Сделать фото", "Отмена"};
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle("Установить фото");
        alertDialog.setItems(source, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0:
                        mPresenter.chooseGallery();
                        break;
                    case 1:
                        mPresenter.chooseCamera();
                        break;
                    case 2:
                        dialog.cancel();
                        break;
                }

            }
        });
        alertDialog.show();
    }

    @Override
    public String getUserName() {
        return String.valueOf(userFullNameEt.getText());
    }

    @Override
    public String getUserPhone() {
        return String.valueOf(userPhoneEt.getText());
    }

    @Override
    public boolean viewOnBackPressed() {
        if (mScreen.getCustomState() == EDIT_STATE) {
            changeState();
            return true;
        } else {
            return false;
        }
    }
    //endregion

    //region================Events================

    @OnClick(R.id.collapsing_toolbar)
    void testEditMode() {
        mPresenter.switchViewState();
    }

    @OnClick(R.id.add_address_btn)
    void clickOnAddress() {
        mPresenter.clickOnAddress();
    }

    @OnClick(R.id.user_avatar_iv)
    void clickOnAvatar() {
        mPresenter.takePhoto();
    }

    @OnClick(R.id.notification_order_sw)
    void clickOnNotificationOrder() {
        mPresenter.switchOrder(notificationOrderSw.isChecked());
    }

    @OnClick(R.id.notification_promo_sw)
    void clickOnNotificationPromo() {
        mPresenter.switchPromo(notificationPromoSw.isChecked());
    }

    //endregion


}
