package yuribtr.skillbranch.ru.mvpauth4.di.components;

import android.content.Context;

import dagger.Component;
import yuribtr.skillbranch.ru.mvpauth4.di.modules.AppModule;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
