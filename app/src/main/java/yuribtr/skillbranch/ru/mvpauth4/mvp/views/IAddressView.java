package yuribtr.skillbranch.ru.mvpauth4.mvp.views;

import yuribtr.skillbranch.ru.mvpauth4.data.storage.dto.UserAddressDto;

public interface IAddressView extends IView {

    void showInputError();

    void showMessage(int messageResId);

    UserAddressDto getUserAddress();
}
