package yuribtr.skillbranch.ru.mvpauth4.mvp.views;

import android.support.annotation.Nullable;

public interface IRootView extends IView {
    void showMessage(String message);

    void showError(Throwable e);

    void showLoad();

    void hideLoad();

    void setMenuItemChecked(String name);

    @Nullable
    IView getCurrentScreen();

    void requestImageFromGallery();

    void requestImageFromCamera();
}
