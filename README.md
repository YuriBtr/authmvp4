This is fourth version of MVP architecture project, which shows how to work with Flow/Mortar.

App was made at learning course [http://skill-branch.ru](http://skill-branch.ru)


**Features**:

New:

- OneActivityAplication

- Loading avatar from gallery and camera (also on targetSDK 24+)

- Swipe delete address from list

Old:

- imitation of catalog load and user enter

- validation of user fields and different types of animation

- support of orientation change

- added dots indicator, goods counter